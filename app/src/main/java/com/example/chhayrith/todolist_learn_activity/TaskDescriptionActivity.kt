package com.example.chhayrith.todolist_learn_activity

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import com.example.chhayrith.todolist_learn_activity.R.id.inputTask

class TaskDescriptionActivity : AppCompatActivity() {

    companion object{
        val EXTRA_TASK_DESCRIPTION = "Task"
    }



    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_description)
    }

    fun doneClicked(view: View){
        val inputText = findViewById(R.id.inputTask) as EditText
        val userInputText = inputText.text.toString()

        if (!userInputText.isEmpty()){
            val result = Intent()
            result.putExtra(EXTRA_TASK_DESCRIPTION, userInputText)
            setResult(Activity.RESULT_OK, result)
        }else{
            setResult(Activity.RESULT_CANCELED)
        }

        finish()
    }
}
