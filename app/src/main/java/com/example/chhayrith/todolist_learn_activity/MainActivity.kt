package com.example.chhayrith.todolist_learn_activity

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.support.annotation.RequiresApi
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

private val ADD_TASK_REQUEST = 1
private val PREFS_TASK = "prefs_task"
private val KEY_TASKS_LIST = "tasks_list"


class MainActivity : AppCompatActivity() {

    companion object{
        private const val TAG_LOG = "MainActivityLog"
        @RequiresApi(Build.VERSION_CODES.N)
        private fun getCurrentTimestamp():String{
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US)
            // simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GTC+7"))
            val now = Date()
            return simpleDateFormat.format(now)
        }
    }

    private val taskList: MutableList<String> = mutableListOf()
//    private val adapter by lazy {
//        makeAdapter(taskList)
//    }
    private val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, taskList)
    private val tickReceiver by lazy{
        makeBroadcastReceiver()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        taskListView.adapter = adapter
        taskListView.onItemClickListener = AdapterView.OnItemClickListener {
            _, _, position, _ -> taskSelected(position)
        }

        val savedList = getSharedPreferences(PREFS_TASK, Context.MODE_PRIVATE).getString(KEY_TASKS_LIST, null)
        if(savedList != null){
            val items = savedList.split(",".toRegex()).dropLastWhile{ it.isEmpty() }.toTypedArray()
            taskList.addAll(items)
            println("saved state of task list array has =>" + taskList)
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    override fun onResume(){
        super.onResume()
        dateTimeTextView.text = getCurrentTimestamp()

        registerReceiver(tickReceiver, IntentFilter(Intent.ACTION_TIME_TICK))
    }

    override fun onPause() {
        super.onPause()

        try{
            unregisterReceiver(tickReceiver)
        }catch(e: IllegalArgumentException){
            Log.e(MainActivity.TAG_LOG, "Time tick Receiver not registered", e)
        }
    }

    override fun onStop(){
        super.onStop()

        val savedList = StringBuilder()
        for(task in taskList){
            savedList.append(task)
            savedList.append(",")
        }

        getSharedPreferences(PREFS_TASK, Context.MODE_PRIVATE).edit().putString(KEY_TASKS_LIST, savedList.toString()).apply()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
    }

    fun addTaskClicked(view: View){
        val intent = Intent(this,  TaskDescriptionActivity::class.java)
        startActivityForResult(intent, ADD_TASK_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == ADD_TASK_REQUEST){
            if(resultCode == Activity.RESULT_OK){
                val task = data?.getStringExtra(TaskDescriptionActivity.EXTRA_TASK_DESCRIPTION)
                println("task input message => " + task)
                task?.let{
                    taskList.add(task)

                    adapter.notifyDataSetChanged()
                }
            }
        }
        println("tasklist array after added => " + taskList)
    }

    private fun makeAdapter(list: List<String>): ArrayAdapter<Any> {
        return ArrayAdapter(this, android.R.layout.simple_list_item_1, list)
    }
    private fun makeBroadcastReceiver(): BroadcastReceiver {
        return object: BroadcastReceiver(){
            @TargetApi(Build.VERSION_CODES.N)
            override fun onReceive(context: Context?, intent: Intent?) {
                if(intent?.action == Intent.ACTION_TIME_TICK){
                    dateTimeTextView.text = getCurrentTimestamp()
                }
            }
        }
    }

    private fun taskSelected(position: Int){
        AlertDialog.Builder(this).setTitle(R.string.alertTitle).setMessage(taskList[position]).setPositiveButton(R.string.delete, { _, _ -> taskList.removeAt(position)
            adapter.notifyDataSetChanged()
        }).setNegativeButton(R.string.cancel, {
            dialog, _ -> dialog.cancel()})
                .create()
                .show()

    }
}

